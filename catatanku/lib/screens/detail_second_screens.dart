import 'package:flutter/material.dart';

class DetailSecondScreens extends StatefulWidget {
  final String? title;
  final String? description;
  final Color? color;
  final Function(Map<String, dynamic> data)? onSave;

  const DetailSecondScreens({
    Key? key,
    required this.title,
    required this.description,
    required this.color,
    required this.onSave,
  }) : super(key: key);

  @override
  State<DetailSecondScreens> createState() => _DetailSecondScreensState();
}

class _DetailSecondScreensState extends State<DetailSecondScreens> {
  TextEditingController _titleController = TextEditingController();
  TextEditingController _descriptionController = TextEditingController();

  Color _selectedColor = Colors.white;

  @override
  void initState() {
    super.initState();
    _titleController.text = widget.title ?? '';
    _descriptionController.text = widget.description ?? '';
    _selectedColor = widget.color ?? Colors.white;
  }

  void _showColorPickerModal() {
    showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30),
          topRight: Radius.circular(30),
        ),
      ),
      builder: (BuildContext context) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            _buildColorOption(Colors.lightGreen[200]!, 'Light Green'),
            _buildColorOption(Colors.deepOrange[300]!, 'Deep Orange'),
            _buildColorOption(Colors.indigo[300]!, 'Indigo'),
            _buildColorOption(Colors.white, 'None'),
          ],
        );
      },
    );
  }

  Widget _buildColorOption(Color color, String label) {
    return ListTile(
      leading: Container(
        width: 30,
        height: 30,
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      horizontalTitleGap: 8.0,
      title: Text(label),
      onTap: () {
        setState(() {
          _selectedColor = color;
        });
        Navigator.pop(context);
      },
    );
  }

  void _handleSave() {
    String title = _titleController.text;
    String description = _descriptionController.text;

    if (title.isNotEmpty || description.isNotEmpty) {
      widget.onSave?.call({
        'title': title,
        'description': description,
        'color': _selectedColor,
      });
      Navigator.pop(context);
    }
  }

  bool _isSaveEnabled() {
    return _titleController.text.isNotEmpty ||
        _descriptionController.text.isNotEmpty;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: _selectedColor,
      floatingActionButton: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisSize: MainAxisSize.min,
        children: [
          FloatingActionButton(
            backgroundColor: Colors.teal,
            heroTag: 'color_picker',
            onPressed: () {
              _showColorPickerModal();
            },
            child: Icon(
              Icons.color_lens,
              color: Colors.white,
            ),
          ),
          SizedBox(height: 10),
          FloatingActionButton(
            backgroundColor: Colors.teal,
            heroTag: 'save',
            onPressed: () {
              _isSaveEnabled() ? _handleSave() : null;
            },
            child: Icon(
              Icons.save,
              color: Colors.white,
            ),
          ),
        ],
      ),
      appBar: AppBar(
        foregroundColor: Colors.white,
        backgroundColor: Colors.teal,
        title: Text(
          'Create Note',
        ),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(
          vertical: 10,
          horizontal: 20,
        ),
        child: Column(
          children: [
            TextField(
              controller: _titleController,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: 'Title',
              ),
            ),
            SizedBox(height: 10),
            TextField(
              controller: _descriptionController,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: 'Description',
              ),
            ),
          ],
        ),
      ),
    );
  }
}
