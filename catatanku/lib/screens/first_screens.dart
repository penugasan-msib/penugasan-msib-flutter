import 'package:catatanku/screens/second_screens.dart';
import 'package:flutter/material.dart';

import '../models/notes.dart';
import 'detail_second_screens.dart';

class FirstScreens extends StatefulWidget {
  @override
  _FirstScreensState createState() => _FirstScreensState();
}

class _FirstScreensState extends State<FirstScreens> {
  bool hasNotes = false;
  bool isListView = false;
  bool isEdit = false;
  List<Notes> notes = [];

  void _showNotification(String title, String description) {
    ScaffoldMessenger.of(context).showSnackBar(isEdit
        ? SnackBar(
            content: Text('Note updated successfully!'),
          )
        : SnackBar(
            content: Text('Note created successfully!'),
          ));
  }

  void _addNote(Notes note) {
    setState(() {
      notes.add(note);
      hasNotes = true;
    });
  }

  void _showEditDeleteModal(int index) {
    showModalBottomSheet(
      showDragHandle: true,
      context: context,
      builder: (BuildContext context) {
        return ListTile(
          leading: Text(
            'Actions',
            style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
          ),
          trailing: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              IconButton.filled(
                style: ButtonStyle(
                  backgroundColor: MaterialStatePropertyAll(Colors.teal),
                ),
                onPressed: () {
                  Navigator.pop(context);
                  isEdit = true;
                  _editNoteModal(index);
                },
                icon: Icon(
                  Icons.edit,
                  color: Colors.white,
                ),
              ),
              IconButton.filled(
                style: ButtonStyle(
                  backgroundColor: MaterialStatePropertyAll(Colors.teal),
                ),
                onPressed: () {
                  Navigator.pop(context);
                  _deleteNote(index);
                },
                icon: Icon(
                  Icons.delete,
                  color: Colors.white,
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  void _editNoteModal(int index) async {
    Map<String, dynamic>? result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => DetailSecondScreens(
          title: notes[index].title,
          description: notes[index].description,
          color: notes[index].colors,
          onSave: (editedData) {
            _editNote(index, editedData);
            _showNotification(editedData['title'], editedData['description']);
          },
        ),
      ),
    );

    if (result != null &&
        result.containsKey('title') &&
        result.containsKey('description')) {
      _editNote(index, result);
      _showNotification(result['title'], result['description']);
    }
  }

  void _editNote(int index, Map<String, dynamic> newNote) {
    setState(() {
      notes[index] = Notes(
        newNote['title'],
        newNote['description'],
        newNote['color'],
      );
    });
  }

  void _deleteNote(int index) {
    setState(() {
      notes.removeAt(index);
      hasNotes = notes.isNotEmpty;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.teal,
        onPressed: () async {
          isEdit = false;
          Map<String, dynamic>? result = await Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => SecondScreens()),
          );
          if (result != null &&
              result.containsKey('title') &&
              result.containsKey('description')) {
            _addNote(
              Notes(
                result['title'],
                result['description'],
                result['color'],
              ),
            );
            _showNotification(
              result['title'],
              result['description'],
            );
          }
        },
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
      ),
      appBar: AppBar(
        foregroundColor: Colors.white,
        actions: [
          if (notes.isNotEmpty)
            IconButton(
              onPressed: () {
                setState(() {
                  isListView = !isListView;
                });
              },
              icon: Icon(
                isListView ? Icons.list : Icons.grid_view,
              ),
            ),
        ],
        backgroundColor: Colors.teal,
        title: Text('Catatanku'),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: _buildNoteContent(),
      ),
    );
  }

  Widget _buildNoteContent() {
    if (notes.isEmpty) {
      return Center(child: Text('Make your first note'));
    } else {
      return Container(
        child: isListView
            ? ListView.builder(
                itemCount: notes.length,
                itemBuilder: (context, index) {
                  final note = notes[index];

                  return GestureDetector(
                    onLongPress: () {
                      _showEditDeleteModal(index);
                    },
                    child: Container(
                      margin: EdgeInsets.only(top: 20),
                      padding: EdgeInsets.all(8.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: note.colors == Colors.white
                            ? Colors.grey
                            : note.colors,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            note.title,
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          SizedBox(height: 5),
                          Text(
                            note.description,
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ],
                      ),
                    ),
                  );
                },
              )
            : GridView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  crossAxisSpacing: 20,
                ),
                itemCount: notes.length,
                itemBuilder: ((context, index) {
                  final note = notes[index];

                  return GestureDetector(
                    onLongPress: () {
                      _showEditDeleteModal(index);
                    },
                    child: Container(
                      height: 80,
                      margin: EdgeInsets.only(top: 20),
                      padding: EdgeInsets.all(8.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: note.colors == Colors.white
                            ? Colors.grey
                            : note.colors,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            note.title,
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          SizedBox(height: 5),
                          Text(
                            note.description,
                            maxLines: 5,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ],
                      ),
                    ),
                  );
                }),
              ),
      );
    }
  }
}
