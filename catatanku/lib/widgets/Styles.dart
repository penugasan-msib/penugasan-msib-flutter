import 'package:flutter/material.dart';

class Styles {
  static BoxDecoration containerButton(BorderRadius borderRadius) {
    return BoxDecoration(
      borderRadius: borderRadius,
      color: Colors.teal,
      boxShadow: [
        BoxShadow(
          color: Colors.teal.withOpacity(0.2),
          spreadRadius: 5,
          blurRadius: 10,
          offset: const Offset(0, 4), // pergeseran bayangan
        ),
      ],
    );
  }

  static InputDecoration inputTitle = InputDecoration(
    border: InputBorder.none,
    hintText: 'Title',
    hintStyle: TextStyle(fontWeight: FontWeight.w900),
  );

  static InputDecoration inputDescription = InputDecoration(
    border: InputBorder.none,
    hintText: 'Write something here...',
  );
}
