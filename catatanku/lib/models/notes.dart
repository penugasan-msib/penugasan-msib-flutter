import 'package:flutter/material.dart';

class Notes {
  final String title;
  final String description;
  final Color colors; // Mengubah Colors menjadi Color

  Notes(this.title, this.description, this.colors);
}