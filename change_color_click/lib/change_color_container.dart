import 'package:flutter/material.dart';

class ChangeColorContainer extends StatefulWidget {
  const ChangeColorContainer({super.key});

  @override
  State<ChangeColorContainer> createState() => _ChangeColorContainerState();
}

class _ChangeColorContainerState extends State<ChangeColorContainer> {
  int _colorIndex = 0; // Variabel untuk melacak indeks warna
  List<Color> _colors = [
    Colors.amber,
    Colors.teal,
    Colors.purpleAccent
  ]; // Daftar warna

  void _changeColor() {
    setState(() {
      _colorIndex =
          (_colorIndex + 1) % _colors.length; // Ganti indeks warna berurutan
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          AnimatedContainer(
            duration: Duration(milliseconds: 500),
            color: _colors[_colorIndex], // Gunakan warna sesuai indeks
            width: 200,
            height: 200,
          ),
          SizedBox(height: 20),
          ElevatedButton(
            style: ElevatedButton.styleFrom(),
            onPressed: _changeColor, // Panggil _changeColor saat tombol ditekan
            child: Text("Change Color"),
          )
        ],
      ),
    );
  }
}
